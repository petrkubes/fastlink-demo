package cz.petrkubes.webviewtest

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.webkit.JavascriptInterface
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import java.net.URLEncoder


class MainActivity : AppCompatActivity() {
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val webview = findViewById<WebView>(R.id.webview)

        webview.settings.javaScriptEnabled = true
        webview.webViewClient = object :WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return super.shouldOverrideUrlLoading(view, request)
            }
        }
        webview.addJavascriptInterface(JSInterfaceHandler(this), "YodleeJS")

        val token = "token"

        val params = Uri.Builder()
            .appendQueryParameter("accessToken", "Bearer $token")
            .appendQueryParameter("app", "10003600")
            .appendQueryParameter("redirectReq", "true")
            .appendQueryParameter("extraParams", URLEncoder.encode("callback=https://google.com/&isMobile=true&deviceType=android", "UTF-8"))
            .build()
            .query ?: ""

        webview.postUrl("https://node.sandbox.yodlee.uk/authenticate/uksandbox", params.encodeToByteArray());
    }

    internal class JSInterfaceHandler(c: Context) {
        @JavascriptInterface
        fun exec(args: String, callBackID: String, secureId: String) {
            Log.d("FL:MESSAGE", "$args, $callBackID, $secureId")
        }
    }
}